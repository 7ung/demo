package com.example.tunghh.demofragment.presenter;

import android.content.Context;

/**
 * Created by TungHH on 7/27/2016.
 */
public interface IButtonHandlePresenter {

    void onButtonShowDialog(Context context);
}
