package com.example.tunghh.demofragment.model;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;

import java.util.HashMap;

/**
 * Created by TungHH on 7/26/2016.
 */
public class ToDoItemProvider extends ContentProvider {

    private static final String TAG = ToDoItemProvider.class.getSimpleName();

    private static final String PROVIDER_NAME = "com.example.tunghh.provider.ToDoItem";
    public static final String URL = "content://" + PROVIDER_NAME + "/todoitems";
    public static final Uri CONTENT_URI = Uri.parse(URL);

    private static final UriMatcher sUrimatcher;

    private static HashMap<String, String> TODOITEM_PROJECTION_MAP
            = new HashMap<String, String>();
    static {
        TODOITEM_PROJECTION_MAP.put(
                ToDoItemContract.ToDoItemEntry._ID,
                ToDoItemContract.ToDoItemEntry._ID );
        TODOITEM_PROJECTION_MAP.put(
                ToDoItemContract.ToDoItemEntry.COLUMN_NAME_TITLE,
                ToDoItemContract.ToDoItemEntry.COLUMN_NAME_TITLE );
        TODOITEM_PROJECTION_MAP.put(
                ToDoItemContract.ToDoItemEntry.COLUMN_NAME_DETAIL,
                ToDoItemContract.ToDoItemEntry.COLUMN_NAME_DETAIL );
        TODOITEM_PROJECTION_MAP.put(
                ToDoItemContract.ToDoItemEntry.COLUMN_NAME_ISDONE,
                ToDoItemContract.ToDoItemEntry.COLUMN_NAME_ISDONE);
        TODOITEM_PROJECTION_MAP.put(
                ToDoItemContract.ToDoItemEntry.COLUMN_NAME_CREATE_DATE,
                ToDoItemContract.ToDoItemEntry.COLUMN_NAME_CREATE_DATE);
    }

    private static final int TO_DO_ITEMS = 1;
    private static final int TO_DO_ITEM_ID = 2;

    static {
        sUrimatcher = new UriMatcher(UriMatcher.NO_MATCH);
        sUrimatcher.addURI(PROVIDER_NAME, "todoitems",TO_DO_ITEMS);
        sUrimatcher.addURI(PROVIDER_NAME, "todoitems/#  ",TO_DO_ITEM_ID);
    }

    private ToDoItemDbHelper mHelper;
    private SQLiteDatabase mSQLiteDb;

    @Override
    public boolean onCreate(){
        Log.d(TAG, "onCreate");

        mHelper = new ToDoItemDbHelper(getContext());
        mSQLiteDb = mHelper.getWritableDatabase();

        return mSQLiteDb != null;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values){
        Log.d(TAG, "insert");

        long rowId = mSQLiteDb.insert(ToDoItemContract.ToDoItemEntry.TABLE_NAME,
                "", values);

        if (rowId > 0)
        {

            Uri _uri = ContentUris.withAppendedId(uri, rowId);
            getContext().getContentResolver().notifyChange(_uri, null);
            return _uri;
        }
        throw new SQLException("Failed to add a record into " + uri);
    }


    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        Log.d(TAG, "update");

        int count = 0;
        switch (sUrimatcher.match(uri)){
            case TO_DO_ITEMS:

                count = mSQLiteDb.update(
                        ToDoItemContract.ToDoItemEntry.TABLE_NAME,
                        values,
                        selection,
                        selectionArgs
                );

                break;
            case TO_DO_ITEM_ID:
                count = mSQLiteDb.update(
                        ToDoItemContract.ToDoItemEntry.TABLE_NAME,
                        values,
                        ToDoItemContract.ToDoItemEntry._ID + " = " + uri.getPathSegments().get(1) +
                                (!TextUtils.isEmpty(selection) ? " AND (" +selection + ')' : ""),
                        selectionArgs
                );
                break;

            default:
                throw new IllegalArgumentException("Unknown URI " + uri );

        }
        getContext().getContentResolver().notifyChange(uri, null);
        return count;
    }

    @Override
    public Cursor query(Uri uri, String[] projection,
                        String selection, String[] selectionArgs, String order){
        Log.d(TAG, "query");

        SQLiteQueryBuilder builder = new SQLiteQueryBuilder();
        builder.setTables(ToDoItemContract.ToDoItemEntry.TABLE_NAME);
        switch (sUrimatcher.match(uri)){
            case TO_DO_ITEMS:
                builder.setProjectionMap(TODOITEM_PROJECTION_MAP);
                break;
            case TO_DO_ITEM_ID:
                builder.appendWhere(ToDoItemContract.ToDoItemEntry._ID
                        + " = " + uri.getPathSegments().get(1));
                break;
            default:
                throw new IllegalArgumentException("Unknown URI " + uri);

        }
        if (order == null || order.isEmpty())
            order = ToDoItemContract.ToDoItemEntry.COLUMN_NAME_CREATE_DATE;

        Cursor s = builder.query(mSQLiteDb,
                projection, selection, selectionArgs, null, null, order);

        s.setNotificationUri( getContext().getContentResolver(), uri);
        return s;
    }
    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        Log.d(TAG, "delete");

        int count = 0;

        switch (sUrimatcher.match(uri)){
            case TO_DO_ITEMS:

                count = mSQLiteDb.delete(
                        ToDoItemContract.ToDoItemEntry.TABLE_NAME,
                        selection,
                        selectionArgs);

                break;

            case TO_DO_ITEM_ID:
                String id = uri.getPathSegments().get(1);
                count = mSQLiteDb.delete(ToDoItemContract.ToDoItemEntry.TABLE_NAME,
                        ToDoItemContract.ToDoItemEntry._ID +  " = " + id +
                                (!TextUtils.isEmpty(selection) ? " AND (" + selection + ')' : ""), selectionArgs);
                break;

            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }

        getContext().getContentResolver().notifyChange(uri, null);
        return count;
    }
    @Nullable
    @Override
    public String getType(Uri uri) {
        Log.d(TAG, "getType");

        switch (sUrimatcher.match(uri)){
            case TO_DO_ITEMS:
                return "vnd.android.cursor.dir/vnd.example.tunghh.provider.ToDoItem";
            case TO_DO_ITEM_ID:
                return "vnd.android.cursor.item/vnd.example.tunghh.provider.ToDoItem";
            default:
                throw new IllegalArgumentException("Unsupported URI: " + uri);
        }
    }


}
