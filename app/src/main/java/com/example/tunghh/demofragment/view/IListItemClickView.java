package com.example.tunghh.demofragment.view;

import com.example.tunghh.demofragment.model.ToDoItem;

/**
 * Created by TungHH on 7/27/2016.
 */
public interface IListItemClickView {
    void handleItemClick(ToDoItem item);
}
