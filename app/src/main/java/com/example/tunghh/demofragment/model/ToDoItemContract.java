package com.example.tunghh.demofragment.model;

import android.provider.BaseColumns;

/**
 * Created by TungHH on 7/26/2016.
 */
public class ToDoItemContract {

     // required empty constuctor
    public ToDoItemContract(){}


    // define table name column name
    public static abstract class ToDoItemEntry implements BaseColumns{
        public static final String TABLE_NAME = "ToDoItem";
        public static final String COLUMN_NAME_TITLE = "Title";
        public static final String COLUMN_NAME_DETAIL = "Detail";
        public static final String COLUMN_NAME_ISDONE = "IsDone";
        public static final String COLUMN_NAME_CREATE_DATE = "CreateDate";

        private static final String COMMA_SEP = ",";

        public static final String TEXT_TYPE = "Text";
        public static final String INT_TYPE = "Integer";

        public static final String SQL_CREATE_ENTRIES =
                "Create table " + TABLE_NAME + " (" +
                        _ID + " INTEGER PRIMARY KEY," +
                        COLUMN_NAME_TITLE + " " +  TEXT_TYPE + COMMA_SEP +
                        COLUMN_NAME_DETAIL + " " +  TEXT_TYPE + COMMA_SEP +
                        COLUMN_NAME_ISDONE + " " +  INT_TYPE + COMMA_SEP +
                        COLUMN_NAME_CREATE_DATE  + " " +  TEXT_TYPE +" )";

        public static final String SQL_DELETE_ENTRIES =
                "DROP TABLE IF EXISTS " + TABLE_NAME;
    }

}
