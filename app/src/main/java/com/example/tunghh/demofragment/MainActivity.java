package com.example.tunghh.demofragment;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTabHost;
import android.support.v4.app.FragmentTransaction;
import android.os.Bundle;
import android.widget.TabHost;

import com.example.tunghh.demofragment.model.ToDoItem;
import com.example.tunghh.demofragment.view.IItemDetailView;
import com.example.tunghh.demofragment.view.IListItemClickView;
import com.example.tunghh.demofragment.view.ItemDetailFragment;
import com.example.tunghh.demofragment.view.ListItemFragment;

import java.util.ArrayList;
import java.util.List;


// I changed something for working
// I'm working on master branch
// I'm working on TungHH branch


public class MainActivity extends FragmentActivity
    implements IListItemClickView{
        static ArrayList<ToDoItem> sTestList = new ArrayList<>();

    private static final String[] sLabel = {"To-do list", "Detail"};

    private FragmentTabHost mTabHost;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        mTabHost = (FragmentTabHost)findViewById(R.id.tabs_host);
        mTabHost.setup(this.getBaseContext(),getSupportFragmentManager(),R.id.main_content);

        FragmentTabHost.TabSpec tabSpec1 = mTabHost.newTabSpec(sLabel[0]);
        tabSpec1.setIndicator(sLabel[0]);
        mTabHost.addTab(tabSpec1,ListItemFragment.class, null);

        tabSpec2 = mTabHost.newTabSpec(sLabel[1]);
        tabSpec2.setIndicator(sLabel[1]);
        mTabHost.addTab(tabSpec2,ItemDetailFragment.class, null);

    }
    FragmentTabHost.TabSpec tabSpec2;
    @Override
    protected void onResume(){
        super.onResume();


    }


    @Override
    public void handleItemClick(ToDoItem item) {
        Bundle bundle = new Bundle();
        bundle.putParcelable("Item", item);

        mTabHost.getTabWidget().removeView(
                mTabHost.getTabWidget().getChildTabViewAt(1)
        );
        TabHost.TabSpec tabSpec = mTabHost.newTabSpec(sLabel[1]);
        tabSpec.setIndicator(sLabel[1]);
        mTabHost.addTab(tabSpec,ItemDetailFragment.class, bundle);
        mTabHost.setCurrentTab(1);

        mClickedItem = item;

    }

    public ToDoItem mClickedItem;
    public void focusFirst(){
        mTabHost.setCurrentTab(0);
    }
}
