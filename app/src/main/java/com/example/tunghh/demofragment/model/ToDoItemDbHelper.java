package com.example.tunghh.demofragment.model;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by TungHH on 7/26/2016.
 */
public class ToDoItemDbHelper extends SQLiteOpenHelper{

    private static final String TAG = ToDoItemDbHelper.class.getSimpleName();

    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "demo_sqlite.db";

    public ToDoItemDbHelper(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        Log.d(TAG, "ToDoItemDbHelper constructor");
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(ToDoItemContract.ToDoItemEntry.SQL_CREATE_ENTRIES);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVer, int newVer) {
        db.execSQL(ToDoItemContract.ToDoItemEntry.SQL_DELETE_ENTRIES);
        onCreate(db);

    }

    public void onDowngrade(SQLiteDatabase db, int oldVer, int newVer){
        Log.d(TAG, "ToDoItemDbHelper ondowngrade");
        onUpgrade(db, oldVer, newVer);
    }


}
