package com.example.tunghh.demofragment.presenter;

import android.content.Context;

import com.example.tunghh.demofragment.view.IShowNewItemDialog;

/**
 * Created by TungHH on 7/27/2016.
 */
public class ButtonHandlePresenter implements IButtonHandlePresenter {

    IShowNewItemDialog iShowDialog;

    public ButtonHandlePresenter(IShowNewItemDialog iView){
        iShowDialog = iView;
    }

    @Override
    public void onButtonShowDialog(Context context) {
        iShowDialog.show(context);
    }
}
