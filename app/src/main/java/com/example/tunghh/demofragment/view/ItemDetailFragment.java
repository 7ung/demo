package com.example.tunghh.demofragment.view;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;

import com.example.tunghh.demofragment.MainActivity;
import com.example.tunghh.demofragment.R;
import com.example.tunghh.demofragment.model.ToDoItem;
import com.example.tunghh.demofragment.presenter.AccessDataPresenter;
import com.example.tunghh.demofragment.presenter.IAccessDataPresenter;
import com.example.tunghh.demofragment.presenter.IItemDetailPresenter;
import com.example.tunghh.demofragment.presenter.ItemDetailPresenter;

/**
 * Created by TungHH on 7/21/2016.
 */
public class ItemDetailFragment extends Fragment
        implements View.OnClickListener, CompoundButton.OnCheckedChangeListener,
        IItemDetailView {

    private ToDoItem mItem;
    private IAccessDataPresenter iDataPresenter;
    private Switch mDoneView;
    private EditText mTitle;
    private EditText mDetail;
    private View mRoot;

    private IItemDetailPresenter iItemPresenter;


    @Override
    public void onCreate(Bundle savedInstance){
        super.onCreate(savedInstance);

        iDataPresenter = new AccessDataPresenter(null);
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstance){
        initLayout(inflater, container);
        initEvent();

        return mRoot;
    }

    @Override
    public void onResume(){
        super.onResume();
        mItem = ((MainActivity)getActivity()).mClickedItem;
        iItemPresenter = new ItemDetailPresenter(this,mItem );

        iItemPresenter.showToView();

    }

    @Override
    public void updateData(ToDoItem item) {
        if (mRoot == null)
            return;
        ((TextView)mRoot.findViewById(R.id.tv_createdDate)).setText(item.getCreatedDate());
        ((EditText)mRoot.findViewById(R.id.tv_title)).setText(item.getTitle());
        ((EditText)mRoot.findViewById(R.id.tv_detail)).setText(item.getDetail());
        ((Switch)mRoot.findViewById(R.id.tv_done)).setChecked(item.getIsDone());

    }

    private void initEvent() {
        (mRoot.findViewById(R.id.btn_back)).setOnClickListener(this);
        (mRoot.findViewById(R.id.btn_update)).setOnClickListener(this);
        (mRoot.findViewById(R.id.btn_delete)).setOnClickListener(this);

        mDoneView.setOnCheckedChangeListener(this);

        mTitle.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                iItemPresenter.setTitle(editable.toString());
            }
        });
        mDetail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                iItemPresenter.setDetail(editable.toString());
            }
        });
    }

    private void initLayout(LayoutInflater inflater, ViewGroup container) {

        mRoot = inflater.inflate(R.layout.fragment_item_detail,container, false);


        mDoneView = (Switch)mRoot.findViewById(R.id.tv_done);
        mTitle = (EditText)mRoot.findViewById(R.id.tv_title);
        mDetail = (EditText)mRoot.findViewById(R.id.tv_detail);


    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_update:
                if (mItem != null)
                    iDataPresenter.onUpdateButtonClick(this.getActivity(), mItem);
                ((MainActivity)getActivity()).focusFirst();
                break;
            case R.id.btn_delete:
                if (mItem != null)
                    iDataPresenter.onDeleteButonClick(this.getActivity(),mItem.getId());
                ((MainActivity)getActivity()).focusFirst();
                break;
            case R.id.btn_back:
                ((MainActivity)getActivity()).focusFirst();

                break;
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

        iItemPresenter.setIsDone(b);
    }
}
