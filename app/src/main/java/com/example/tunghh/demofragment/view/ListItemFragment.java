package com.example.tunghh.demofragment.view;


import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;

import com.example.tunghh.demofragment.MainActivity;
import com.example.tunghh.demofragment.R;
import com.example.tunghh.demofragment.databinding.TodoItemBinding;
import com.example.tunghh.demofragment.model.ToDoItem;
import com.example.tunghh.demofragment.presenter.AccessDataPresenter;
import com.example.tunghh.demofragment.presenter.ButtonHandlePresenter;
import com.example.tunghh.demofragment.presenter.IAccessDataPresenter;
import com.example.tunghh.demofragment.presenter.IButtonHandlePresenter;
import com.example.tunghh.demofragment.presenter.IListItemClickPresenter;
import com.example.tunghh.demofragment.presenter.ListItemClickPresenter;

import java.util.ArrayList;

/**
 * Created by TungHH on 7/21/2016.
 */
public class ListItemFragment extends Fragment
        implements AdapterView.OnItemClickListener, View.OnClickListener,
            INoteView, IShowNewItemDialog{

    private ListView mListView;
    private BaseAdapter mAdapter;

    IAccessDataPresenter iPresenter;
    private ArrayList<ToDoItem> mListItem = new ArrayList<>();

    public static void create(FragmentActivity context,@IdRes int resID){
        FragmentManager manager = context.getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.add(resID, new ListItemFragment());
        transaction.commit();
    }

    @Override
    public void onCreate(Bundle savedInstance) {
        super.onCreate(savedInstance);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup containter, Bundle savedInstance) {
        View v = inflater.inflate(R.layout.fragment_todolist, containter, false);

        mListView = (ListView) v.findViewById(R.id.lv_todo_list);
        mListView.setOnItemClickListener(this);

        mListItem = new ArrayList<>();

        (v.findViewById(R.id.btn_create)).setOnClickListener(this);

        iPresenter = new AccessDataPresenter(this);
        return v;

    }

    @Override
    public void onStart() {
        super.onStart();
        iPresenter.onLoad(this.getActivity());
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

        IListItemClickPresenter presenter = new ListItemClickPresenter((MainActivity)this.getActivity());
        presenter.onItemClick((ToDoItem)mAdapter.getItem(i));
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_create:
                IButtonHandlePresenter buttonHandlePresenter = new ButtonHandlePresenter(this);
                buttonHandlePresenter.onButtonShowDialog(this.getActivity());
                break;
        }
    }

    @Override
    public void refreshListItem(ArrayList<ToDoItem> listItems) {
        mListItem.clear();
        for (ToDoItem item : listItems) {
            mListItem.add(item);
        }
        mAdapter = new ToDoListAdapter(
                this.getActivity(),
                mListItem);
        mListView.setAdapter(mAdapter);
    }

    @Override
    public void show(Context context) {
        NewItemDialog dialog = new NewItemDialog(this.getActivity()) {
            @Override
            void onResult(ToDoItem item) {
                iPresenter.onAddNewButtonClick(
                        ListItemFragment.this.getActivity(),
                        item);
            }
        };
        dialog.show();
    }

    private class ToDoListAdapter extends BaseAdapter {

        private ArrayList<ToDoItem> mToDoList = new ArrayList<>();
        private Context mContext;
        private LayoutInflater mInflater;

        public ToDoListAdapter(Context context, ArrayList<ToDoItem> toDoItems) {
            mToDoList.addAll(toDoItems);
            mContext = context;
            mInflater = (LayoutInflater) context.
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return mToDoList.size();
        }

        @Override
        public Object getItem(int i) {
            return mToDoList.get(i);
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(int i, View convertView, ViewGroup parent) {
            //mInflater.inflate(R.layout.todo_item,viewGroup,true);
            if (convertView == null) {
                TodoItemBinding binding = DataBindingUtil.inflate(
                        mInflater, R.layout.todo_item, parent, false);
                binding.setItem(mToDoList.get(i));

                return binding.getRoot();
            }

            return convertView;
        }
    }

}
