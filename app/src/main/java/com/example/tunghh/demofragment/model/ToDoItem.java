package com.example.tunghh.demofragment.model;

import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.InputType;

import java.lang.reflect.InvocationTargetException;

/**
 * Created by TungHH on 7/21/2016.
 */
public class ToDoItem extends BaseObservable implements Parcelable, Cloneable {

    private String mTitle;
    private String mDetail;
    private boolean mIsDone;
    private String mCreatedDate;
    private int mId;
    @Bindable
    public String getTitle(){
        return mTitle;
    }


    @Bindable
    public boolean getIsDone(){
        return mIsDone;
    }


    @Bindable
    public String getDetail(){
        return mDetail;
    }
    @Bindable
    public String getCreatedDate(){
        return mCreatedDate;
    }

    public ToDoItem setDetail(String detail){
        mDetail = detail;
        return this;
    }

    public ToDoItem setTitle(String title){
        mTitle = title;
        return this;
    }

    public ToDoItem setIsDone(boolean isDone){
        mIsDone = isDone;
        return this;
    }

    public ToDoItem setCreatedDate(String date){
        mCreatedDate = date;
        return this;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(mTitle);
        parcel.writeString(mDetail);
        parcel.writeInt((mIsDone)?1:0);
        parcel.writeString(mCreatedDate);
        parcel.writeInt(mId);
    }

    public static final Parcelable.Creator<ToDoItem> CREATOR = new Creator<ToDoItem>() {
        @Override
        public ToDoItem createFromParcel(Parcel parcel) {
            ToDoItem item = new ToDoItem();
            item.setTitle(parcel.readString());
            item.setDetail(parcel.readString());
            item.setIsDone((parcel.readInt() == 1) ? true : false);
            item.setCreatedDate(parcel.readString());
            item.setId(parcel.readInt());
            return item;
        }

        @Override
        public ToDoItem[] newArray(int i) {
            return new ToDoItem[0];
        }
    };

    public Object clone()throws CloneNotSupportedException{
        ToDoItem item = new ToDoItem();
        item.setDetail(this.getDetail());
        item.setTitle(this.getTitle());
        item.setIsDone(this.getIsDone());
        item.setCreatedDate(this.getCreatedDate());
        item.setId(this.getId());
        return item;
    }

    public int getId() {
        return mId;
    }

    public void setId(int mId) {
        this.mId = mId;
    }
}
