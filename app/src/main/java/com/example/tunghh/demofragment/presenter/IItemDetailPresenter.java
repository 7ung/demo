package com.example.tunghh.demofragment.presenter;

import com.example.tunghh.demofragment.model.ToDoItem;

/**
 * Created by TungHH on 7/28/2016.
 */
public interface IItemDetailPresenter {

    void setTitle(String title);
    void setDetail(String detail);
    void setCreateDate(String createDate);
    void setIsDone(boolean isDone);
    void setId(int id);
    void showToView();
}
