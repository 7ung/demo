package com.example.tunghh.demofragment.model;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by TungHH on 7/27/2016.
 */
public class ToDoItemLoader {

    private final Context mContext;

    public ToDoItemLoader(Context context){
        mContext = context;
    }

    public ArrayList<ToDoItem> loadAll(){

        Uri todoitem = Uri.parse(ToDoItemProvider.URL);
        ArrayList<ToDoItem> rs = new ArrayList<>();
        Cursor c = mContext.getContentResolver().query(todoitem, null, null, null,null);
        if (c.moveToFirst()){
            do {
                ToDoItem item = new ToDoItem();

                item.setId(c.getInt(c.getColumnIndex(
                        ToDoItemContract.ToDoItemEntry._ID )));
                item.setTitle(c.getString(c.getColumnIndex(
                        ToDoItemContract.ToDoItemEntry.COLUMN_NAME_TITLE)));

                item.setDetail(c.getString(c.getColumnIndex(
                        ToDoItemContract.ToDoItemEntry.COLUMN_NAME_DETAIL)));

                item.setCreatedDate(c.getString(c.getColumnIndex(
                        ToDoItemContract.ToDoItemEntry.COLUMN_NAME_CREATE_DATE)));

                item.setIsDone(
                        (c.getInt(c.getColumnIndex(
                                ToDoItemContract.ToDoItemEntry.COLUMN_NAME_ISDONE))) == 1 ? true : false);
                rs.add(item);

            }
            while (c.moveToNext());
        }
        return rs;
    }

    public boolean addNewItem(ToDoItem item) {
        ContentValues values = new ContentValues();
        values.put(ToDoItemContract.ToDoItemEntry.COLUMN_NAME_TITLE, item.getTitle());
        values.put(ToDoItemContract.ToDoItemEntry.COLUMN_NAME_DETAIL, item.getDetail());
        values.put(ToDoItemContract.ToDoItemEntry.COLUMN_NAME_CREATE_DATE, item.getCreatedDate());
        int t = item.getIsDone() ? 1 : 0;
        values.put(ToDoItemContract.ToDoItemEntry.COLUMN_NAME_ISDONE, t);

        Uri uri = mContext.getContentResolver().insert(
                ToDoItemProvider.CONTENT_URI,
                values);
        if (uri == null)
            return false;
        else
            return true;
    }

    public void deleteItem(int id){
        Uri uri = Uri.parse(ToDoItemProvider.URL);
        mContext.getContentResolver().delete(uri, "_id=?",new String[]{ String.valueOf(id)});
    }

    public int updateItem(ToDoItem item){
        ContentValues values = new ContentValues();
        values.put(ToDoItemContract.ToDoItemEntry.COLUMN_NAME_TITLE, item.getTitle());
        values.put(ToDoItemContract.ToDoItemEntry.COLUMN_NAME_DETAIL, item.getDetail());
        values.put(ToDoItemContract.ToDoItemEntry.COLUMN_NAME_CREATE_DATE, item.getCreatedDate());
        int t = item.getIsDone() ? 1 : 0;
        values.put(ToDoItemContract.ToDoItemEntry.COLUMN_NAME_ISDONE, t);

        Uri uri = Uri.parse(ToDoItemProvider.URL);

        return mContext.getContentResolver().update(
                uri,
                values,
                "_id=?",
                new String[]{String.valueOf(item.getId())});
    }
}
