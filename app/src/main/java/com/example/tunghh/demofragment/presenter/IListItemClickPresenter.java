package com.example.tunghh.demofragment.presenter;

import com.example.tunghh.demofragment.model.ToDoItem;

/**
 * Created by TungHH on 7/27/2016.
 */
public interface IListItemClickPresenter {
    void onItemClick(ToDoItem item);
}
