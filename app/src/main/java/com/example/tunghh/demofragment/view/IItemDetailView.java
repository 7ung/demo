package com.example.tunghh.demofragment.view;

import com.example.tunghh.demofragment.model.ToDoItem;

/**
 * Created by TungHH on 7/28/2016.
 */
public interface IItemDetailView {
    void updateData(ToDoItem item);

}
