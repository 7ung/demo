package com.example.tunghh.demofragment.view;

import android.app.Dialog;
import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.tunghh.demofragment.R;
import com.example.tunghh.demofragment.model.ToDoItem;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * Created by TungHH on 7/27/2016.
 */
public interface IShowNewItemDialog {

    void show(Context context);

    abstract class NewItemDialog extends Dialog implements TextWatcher, View.OnClickListener {

        private final EditText mTitle;
        private final EditText mDetail;
        private final Button mOK;

        private ToDoItem mItem;

        abstract void onResult(ToDoItem item);


        public NewItemDialog(Context context) {
            super(context);
            setContentView(R.layout.dialog_new_item);

            Calendar calendar = GregorianCalendar.getInstance();
            SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");

            mItem = new ToDoItem();
            mItem.setIsDone(false);
            mItem.setCreatedDate(format.format(calendar.getTime()));

            mTitle = (EditText)findViewById(R.id.tb_title);
            mDetail = (EditText)findViewById(R.id.tb_detail);
            mOK = (Button)findViewById(R.id.btn_ok);

            mTitle.addTextChangedListener(this);
            mDetail.addTextChangedListener(this);
            mOK.setOnClickListener(this);
        }


        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void afterTextChanged(Editable editable) {
            mItem.setDetail(mDetail.getText().toString());
            mItem.setTitle(mTitle.getText().toString());
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.btn_ok:
                    onResult(mItem);
                    this.dismiss();
                    break;
            }
        }

    }
}
