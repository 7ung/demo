package com.example.tunghh.demofragment.presenter;

import android.content.Context;

import com.example.tunghh.demofragment.model.ToDoItem;

import java.util.ArrayList;

/**
 * Created by TungHH on 7/27/2016.
 */
public interface IAccessDataPresenter {

    void onLoad(Context context);
    void onAddNewButtonClick(Context context, ToDoItem item);
    void onDeleteButonClick(Context context, int id);
    void onUpdateButtonClick(Context context, ToDoItem item);

}
