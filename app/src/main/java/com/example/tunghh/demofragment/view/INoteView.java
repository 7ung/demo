package com.example.tunghh.demofragment.view;

import android.widget.ListView;

import com.example.tunghh.demofragment.model.ToDoItem;

import java.util.ArrayList;
import java.util.Objects;

/**
 * Created by TungHH on 7/27/2016.
 */
public interface INoteView {

    void refreshListItem(ArrayList<ToDoItem> item);

}
