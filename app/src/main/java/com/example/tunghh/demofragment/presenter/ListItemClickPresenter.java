package com.example.tunghh.demofragment.presenter;

import com.example.tunghh.demofragment.model.ToDoItem;
import com.example.tunghh.demofragment.view.IListItemClickView;

/**
 * Created by TungHH on 7/27/2016.
 */
public class ListItemClickPresenter implements IListItemClickPresenter {
    private IListItemClickView iListItemClickView;

    public ListItemClickPresenter(IListItemClickView iView) {
        iListItemClickView = iView;
    }

    @Override
    public void onItemClick(ToDoItem item) {
        iListItemClickView.handleItemClick(item);
    }
}
