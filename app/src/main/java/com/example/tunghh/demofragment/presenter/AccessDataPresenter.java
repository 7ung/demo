package com.example.tunghh.demofragment.presenter;

import android.app.Activity;
import android.content.Context;

import com.example.tunghh.demofragment.model.ToDoItem;
import com.example.tunghh.demofragment.model.ToDoItemLoader;
import com.example.tunghh.demofragment.view.INoteView;

import java.util.ArrayList;

/**
 * Created by TungHH on 7/27/2016.
 */
public class AccessDataPresenter implements IAccessDataPresenter {

    INoteView iNoteView ;

    public AccessDataPresenter(INoteView noteView){
        iNoteView = noteView;
    }

    @Override
    public void onLoad(Context context) {
        ToDoItemLoader loader = new ToDoItemLoader(context);
        iNoteView.refreshListItem(loader.loadAll());
    }

    @Override
    public void onAddNewButtonClick(Context context, ToDoItem item){
        ToDoItemLoader loader = new ToDoItemLoader(context);
        loader.addNewItem(item);
        iNoteView.refreshListItem(loader.loadAll());
    }

    @Override
    public void onDeleteButonClick(Context context, int id){
        ToDoItemLoader loader = new ToDoItemLoader(context);
        loader.deleteItem(id);

    }

    @Override
    public void onUpdateButtonClick(Context context, ToDoItem item){
        ToDoItemLoader loader = new ToDoItemLoader(context);
        loader.updateItem(item);
    }



}
