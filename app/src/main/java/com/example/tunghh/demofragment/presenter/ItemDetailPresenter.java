package com.example.tunghh.demofragment.presenter;

import com.example.tunghh.demofragment.model.ToDoItem;
import com.example.tunghh.demofragment.view.IItemDetailView;
import com.example.tunghh.demofragment.view.IListItemClickView;

/**
 * Created by TungHH on 7/28/2016.
 */
public class ItemDetailPresenter implements IItemDetailPresenter {

    IItemDetailView mView;
    ToDoItem mModelItem;

    public ItemDetailPresenter(IItemDetailView iView){
        mView = iView;
        mModelItem = new ToDoItem();
    }
    public ItemDetailPresenter(IItemDetailView iView,ToDoItem model ){
        mView = iView;
        if (model == null)
            mModelItem = new ToDoItem();
        else
            mModelItem = model;
    }

    @Override
    public void setTitle(String title) {
        mModelItem.setTitle(title);
    }

    @Override
    public void setDetail(String detail) {
        mModelItem.setDetail(detail);
    }

    @Override
    public void setCreateDate(String createDate) {
        mModelItem.setCreatedDate(createDate);
    }

    @Override
    public void setIsDone(boolean isDone) {
        mModelItem.setIsDone(isDone);
    }

    @Override
    public void setId(int id) {
        mModelItem.setId(id);
    }

    @Override
    public void showToView( ) {
        mView.updateData(mModelItem);
    }
}
