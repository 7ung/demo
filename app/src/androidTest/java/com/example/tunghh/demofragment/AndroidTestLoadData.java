package com.example.tunghh.demofragment;

import android.app.Application;
import android.content.Context;
import android.test.ApplicationTestCase;
import android.test.mock.MockApplication;
import android.test.mock.MockContext;

import com.example.tunghh.demofragment.model.ToDoItem;
import com.example.tunghh.demofragment.model.ToDoItemLoader;


import org.hamcrest.core.IsNot;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricGradleTestRunner;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

import static org.junit.Assert.assertThat;

/**
 * Created by TungHH on 7/28/2016.
 */
@RunWith(RobolectricGradleTestRunner.class)
public class AndroidTestLoadData  {

    @Before
    public void setup(){
    }

    @Test
    public void loadAllData(){
        MainActivity activity = Robolectric.setupActivity(MainActivity.class);

        ToDoItemLoader mLoader = new ToDoItemLoader(activity);
        ArrayList<ToDoItem> list = mLoader.loadAll();

        assertThat((Collection)list, IsNot.<Collection>not(Collections.emptyList()));
    }
}
